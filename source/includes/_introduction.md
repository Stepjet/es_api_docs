# Introduction

Easync lets you buy things from popular online retailers, including Amazon.com, with a single POST request. Easync also lets you get prices and descriptive information about products from supported retailers.

###Quick start
1. Make an account at [easync.io](https://easync.io/).
2. Follow the instructions in the [create an order section](#create-an-order) from the documentation below to place your first order.
