# Place an order

## Create an order

Easync offers the underlying API for apps that need real-time order placing capabilities. With a single POST request, you can order an item from one of our supported retailers. Making an order request will start an order. You’ll receive a `request_id` in the POST body’s response which you’ll then use for [retrieving the status of the order](#retrieving-an-order).

>Example create an order request

```shell
curl "https://api.easync.io/v1/orders" \
  -u <client_token>: \
  -d '{
  "retailer": "amazon",
  "products": [
    {
      "product_id": "0923568964",
      "quantity": 1,
      "variants": [
        {
          "dimension": "color",
          "value": "Red",
        }
      ]
    }
  ],
  "max_price": 2300,
  "shipping_address": {
    "first_name": "Tim",
    "last_name": "Beaver",
    "address_line1": "77 Massachusetts Avenue",
    "address_line2": "",
    "zip_code": "02139",
    "city": "Cambridge",
    "state": "MA",
    "country": "US",
    "phone_number": "5551230101"
  },
  "is_gift": true,
  "gift_message": "Here is your package, Tim! Enjoy!",
  "shipping": {
    "order_by": "price",
    "max_days": 5,
    "max_price": 1000
  },
  "payment_method": {
    "name_on_card": "Ben Bitdiddle",
    "number": "5555555555554444",
    "security_code": "123",
    "expiration_month": 1,
    "expiration_year": 2020,
    "use_gift": false
  },
  "billing_address": {
    "first_name": "William",
    "last_name": "Rogers",
    "address_line1": "84 Massachusetts Ave",
    "address_line2": "",
    "zip_code": "02139",
    "city": "Cambridge",
    "state": "MA",
    "country": "US",
    "phone_number": "5551234567"
  },
  "retailer_credentials": {
    "email": "timbeaver@gmail.com",
    "password": "myRetailerPassword"
  },
  "webhooks": {
    "order_placed": "http://mywebsite.com/easync/order_placed",
    "order_failed": "http://mywebsite.com/easync/order_failed",
    "tracking_obtained": "http://mywebsite.com/easync/tracking_obtained"
  },
  "client_notes": {
    "our_internal_order_id": "abc123",
    "any_other_field": ["any value"]
  }
}'
```

>Example create an order response

```json
{
  "request_id": "3f1c939065cf58e7b9f0aea70640dffc"
}
```


###Required attributes

Attribute | Type | Description 
--------- | --------- | -------
retailer | String | The retailer code of the supported retailer
products | List | A list of [product objects](#product-object) that should be ordered
shipping_address | Object | An [address object](#address-object) to which the order will be delivered
shipping_method | String | The desired shipping method for the object. Available methods are `cheapest` (always select the cheapest method available), `fastest` (always select the fastest method available), or `free` (which will fail for items without some sort of free shipping). You must provide either this or the `shipping` attribute, but not both.
shipping | Object | A [shipping object](#shipping-object) with information as to which shipping method to use. You must provide either this or the `shipping_method` attribute, but not both.
billing_address | Object | An [address object](#address-object) for the person associated with the credit card
payment_method | Object | A [payment method](#payment-method-object) object containing payment information for the order
retailer_credentials | Object | A [retailer credentials](#retailer-credentials-object) object for logging into the retailer with a preexisting account


###Optional attributes

Attribute | Type | Description 
--------- | --------- | -------
gift_message | String | A message to include on the packing slip for the recipient. Must be no more than 240 characters, or 9 lines.
is_gift | Boolean | Whether or not this order should be placed as a gift. Typically, retailers will exclude the price of the items on the receipt if this is set.
max_price | Number | The maximum price in cents for the order. If the final price exceeds this number, the order will not go through and will return a `max_price_exceeded` error.
webhooks | Object | A [webhooks object](#webhooks-object) including URLs that will receive POST requests after particular events have finished
client_notes | Object | Any metadata to store on the request for future use. This object will be passed back in the response.
promo_codes | Array | A list of promotion codes to use at checkout.
ignore_invalid_promo_code | Boolean | (`nordstrom` only). Continue with checkout even if promotion codes are invalid. Default is `false`.
po_number | Number | (`amazon` business accounts only). Adds a purchase order number to the order.



##Retrieving an order

>Example retrieve an order request

```shell
curl "https://api.easync.io/v1/orders/3f1c939065cf58e7b9f0aea70640dffc" \
  -u <client_token>:
```

>Example retrieve an order response (request processing)

```json
{
  "_type": "error",
  "code": "request_processing",
  "message": "Request is currently processing and will complete soon.",
  "data": {}
}
```

To see the status of an order, you can retrieve it using the request id you obtained from your order request, and placing it in a GET request URL. Orders usually take a while to process. While your order is processing, the response will return an error with code type `request_processing`.

Once the request completes, the retrieve an order response should either return a response of type `order_response` or `error`. An error response body will contain a `code` and a `message`. The code indicates the error that occurred, while the message provides a more detailed description of the error. Any extra details about the error will be provided in the `data` object. For a full list of errors, see the [Errors section](#errors).


###Order response attributes

>Example retrieve an order response (order response)

```json
{
  "_type" : "order_response",
  "price_components" : {
    "shipping" : 0,
    "subtotal" : 1999,
    "tax" : 0,
    "total" : 1999
  },
  "merchant_order_ids" : [
    {
      "merchant_order_id" : "112-1234567-7272727",
      "merchant" : "amazon",
      "account" : "timbeaver@gmail.com",
      "placed_at" : "2014-07-02T23:51:08.366Z"
    }
  ],
  "tracking" : [
    {
      "product_id" : "0923568964",
      "merchant_order_id" : "112-1234567-7272727",
      "carrier" : "Fedex",
      "tracking_number" : "9261290100129790891234",
      "obtained_at" : "2014-07-03T23:22:48.165Z"
    }
  ],
  "request" : {
    ...
  }
}
```

Attribute | Type | Description 
--------- | --------- | -------
price_components | Object | A [price components object](#price-components-object) which contains details about the price of the final order
merchant_order_ids | Array | A [merchant order ids object](#merchant-order-ids-object) which contains details about the retailer’s order identifiers
tracking | Array | An array of [tracking objects](#tracking-object) that contain the order’s tracking information. In most cases, this field will not be populated immediately after the order is placed and will only be available later after tracking is updated by the retailer. Once tracking has been obtained, a POST request will be sent to the <code class="prettyprint">tracking_obtained</code> field of the <a href="#webhooks-object">webhooks object</a> from the request if set.
request | Object | The original request that was sent to the Easync API


##Selecting shipping

Ordering on the Easync API can be complicated due to all the potential shipping options available. Generally, faster shipping will cost more money, so you must decide how fast you’d like your items or how much money to pay for shipping. Easync provides a number of options to customize your shipping speed and cost formulas.

Since different items will have different shipping options, you can use a product’s [seller selection criteria](#seller-selection-criteria) to specify `max_handling_days`. This will filter the list of potential offers down to those that will arrive within a certain number of days. The Easync API will then select the cheapest offer that matched all of your seller selection criteria to make a purchase. For example, if you specified `"max_handling_days": 6`, then any offer whose latest delivery date is greater than 6 days from now would be excluded from your buying selection. Thus, if two sellers are offering the same product, but one has a guaranteed delivery date 10 days away and the other seller has a guaranteed delivery date 5 days away, the second seller’s offer would be selected.

You may also use the [shipping parameter](#shipping-object) on an order to select a shipping option once a product has been selected. Instead of filtering by the different offers, like the seller selection criteria, the `shipping` parameter will choose the shipping speed on the selected offer. For example, if you set `"max_days": 5` on the `shipping` parameter, the Easync API would attempt to select the cheapest shipping method that took less than 5 days. Thus, if there was a shipping method that took 3 days and cost $10 and another shipping method that took 7 days but cost $2, the first shipping option would be selected.

