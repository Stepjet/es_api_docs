# Supported retailers

The table below shows the endpoints available for each retailer. We can add additional retailers upon request – [contact us](mailto://support@easync.io) for a quote for a particular retailer.

Name | Retailer Code | Orders | Product Details | Product Prices
--------- | --------- | ------- | ------- | -----------
AliExpress | aliexpress | Y | Y | Y
Amazon | amazon | Y | Y | Y
Amazon United Kingdom | amazon_uk | Y | Y | Y
Amazon Canada | amazon_ca | Y | Y | Y
Amazon Mexico | amazon_mx | Y | Y | Y
Costco | costco | Y | Y | Y
East Dane | eastdane | Y |  | 
Google Shopping | google_shopping |  | Y | Y
Newegg | newegg | Y |  | 
Nordstrom | nordstrom | Y |  | 
Overstock | overstock |  | Y | Y
Shopbop | shopbop | Y |  | 
Walmart | walmart | Y | Y | Y